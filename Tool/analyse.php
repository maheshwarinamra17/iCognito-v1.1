<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<script type="text/javascript" src="scripts/flowplayer-3.2.11.min.js"></script>
<title>iCognito v1.1</title>
<link rel="shortcut icon" href="images/favicon.png" />
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>

	<div id="container"><!--container starts-->

	<div><!--Header starts-->
		<img src="images/logo_name.png" style="margin-left:1%;margin-bottom:0.5%;"/>
		<a href="http://iiit.ac.in/" target="_blank"><img src="images/iiith_logo.jpg" style="margin-left:55%;"/></a>		

	</div><!--Header ends-->

	<div><!--Navigation Bar starts-->	
	<ul id="nav">
	<li><a href="index.html">Home</a></li>
	<li class="current"><a href="#">File</a>
		<ul>
			<li><a href="nproject.html">New</a></li>
			<li><a href="oproject.html">Open</a></li>
			<li><a href="#" onclick="setTimeout(function(){var ww = window.open(window.location, '_self'); ww.close(); }, 500);">Quit</a></li>
		</ul>	

	</li>
	</li>	
	<li><a href="analyse.html">Analysis</a></li>
	<li><a href="#">Export</a>
		<ul>
			<li><a href="exporth.html">Export as HTML</a></li>
			<li><a href="#">Export as PDF</a></li>
		</ul>
	</li>
	<li><a href="#">Help</a>
		<ul>
			<li><a href="user_guide.html">User Guide</a></li>
			<li><a href="about.html">About</a></li>
		</ul>	

	</li>
	</ul>
	</div><!--Navigation Bar ends-->

	<div id="analyse"><!--Details Section-->
		<table>
		<tr>
		<td id="antb">
			<h3 style="text-align:center;">Project Details</h3>
			<form action="analyse.php" method="POST">
			<table cellspacing="15">
							<tr><td style="font-weight:bold;">Select Project</td><td>:</td>
								<td>
									<select name="proj">
										<option value="1">A qualitative Study on Reaction time</option>
										<option value="2">A different Cognitive Study</option>
										<option value="opel">Opel</option>
										<option value="audi">Audi</option>
									</select>
								</td>
							</tr>
							<tr><td style="font-weight:bold;">Select Subject</td><td>:</td>
								<td>
									<select>
										<option value="volvo">Namra Maheshwari</option>
										<option value="saab">Amitash Ojha</option>
										<option value="opel">Opel</option>
										<option value="audi">Audi</option>
									</select>
								</td>
							</tr>
							<tr><td></td><td></td><td><input type="Submit" name="Submit" value="Submit"/></td></tr>
				
			</table>
			</form>
			<hr style="width:90%;">
			<table cellspacing="15" >
			<?php
				$p=1;
				$p=$_POST['proj'];
				if($p==1)
				{echo '<tr><td style="font-weight:bold;">Title</td><td>:</td><td>A qualitative Study on Reaction time</td></tr>
			<tr><td style="font-weight:bold;">Subject</td><td>:</td><td>Namra Maheshwari</td></tr>			
			<tr><td style="font-weight:bold;">Description</td><td>:</td><td style="text-align:justify;">Reaction time is a measure of how quickly an organism can respond to a particular stimulus. Reaction time has been widely studied, as its practical implications may be of great consequence, e.g. a slower than normal reaction time while driving can have grave results.</td></tr>
			</table>';}
				if($p==2)
				{echo '<tr><td style="font-weight:bold;">Title</td><td>:</td><td>A different Cognitive Study</td></tr>
			<tr><td style="font-weight:bold;">Subject</td><td>:</td><td>Amitash Ojha</td></tr>			
			<tr><td style="font-weight:bold;">Description</td><td>:</td><td style="text-align:justify;">Reaction time is a measure of how quickly an organism can respond to a particular stimulus. Reaction time has been widely studied, as its practical implications may be of great consequence, e.g. a slower than normal reaction time while driving can have grave results.</td></tr>
			</table>';}
			
			?>
			<hr style="width:90%;">			
			<table cellspacing="15" >			
			<tr><td style="font-weight:bold;">Maximum</td><td>:</td><td>1.48945</td><td style="width:50px;"></td><td style="font-weight:bold;">Other 3</td><td>:</td><td>0.0000</td></tr>
			<tr><td style="font-weight:bold;">Minimum</td><td>:</td><td>1.03744</td><td></td><td style="font-weight:bold;">Other 4</td><td>:</td><td>0.0000</td></tr>
			<tr><td style="font-weight:bold;">Mean</td><td>:</td><td>1.03744</td><td></td><td style="font-weight:bold;">Other 5</td><td>:</td><td>0.0000</td></tr>
			<tr><td style="font-weight:bold;">Other 1</td><td>:</td><td>0.00000</td><td></td><td style="font-weight:bold;">Other 6</td><td>:</td><td>0.0000</td></tr>
			<tr><td style="font-weight:bold;">Other 2</td><td>:</td><td>0.00000</td><td></td><td style="font-weight:bold;">Other 7</td><td>:</td><td>0.0000</td></tr>			
			</table>
			
		</td>
		
		<td>
			<?php
				$p=1;
				$p=$_POST['proj'];
				if($p==1)
				{echo '<img src="graphs/Plot_1_b.png" title="graph" style="float:right;"/>
				<img src="graphs/histogram_1.png" title="graph" style="float:right;"/>';}
				if($p==2)
				{echo '<img src="graphs/Plot_2_b.png" title="graph" style="float:right;"/>
				<img src="graphs/histogram.png" title="graph" style="float:right;"/>';}
			?>
		
		</td>
			</tr></table>
	</div><!--Details section ends-->
		
	</div><!--container ends-->

	<div id="footer">
		Copyright &copy; <a href="http://web.iiit.ac.in/~namra.maheshwari/" style="" target="_blank">Namra Maheshwari</a>. All rights reserved.<br/>
		This website is best viewed in 1024x768 or higher screen mode.
	</div>

</body>
</html>
