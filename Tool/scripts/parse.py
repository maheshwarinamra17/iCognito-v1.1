# iCognito v1.1 Plotting Script Python
# By : Namra Maheshwari
# Date: 18/09/2012

import os
# Open input data file
filen=raw_input()
inp=open(filen,"r")

# Open output file for E: Skin Cond
f1=open("Plot_1.dat","w")
# Open output file for F: Temp
f2=open("Plot_2.dat","w")
# Open output file for G: Abd Resp
f3=open("Plot_3.dat","w")

# Read lines 
x=inp.readlines()

# Start from line no.9
for i in xrange(8,len(x)):
	k=x[i].split(",")
	if(len(k)>2):
		f1.write(k[0]+" "+k[1]+"\n")
		f2.write(k[0]+" "+k[2]+"\n")
		f3.write(k[0]+" "+k[3].strip()+"\n")
	else:
		continue

# Plot graphs
c1="gnuplot -e \"filename='"+filen+"'\" plot_script_small.gnu"
c2="gnuplot -e \"filename='"+filen+"'\" plot_script_big.gnu"
os.system(c1)
os.system(c2)

