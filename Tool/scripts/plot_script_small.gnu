# Graphs 945 by 100
set xtics 10
set grid
unset key
unset ytics
set tmarg 0.5
set bmarg 1.5
set terminal png nocrop enhanced size 945,100
f1="../graphs/".filename."_Plot_1_s.png"
f2="../graphs/".filename."_Plot_2_s.png"
f3="../graphs/".filename."_Plot_3_s.png"


set ylabel "S"
set output f1
plot "Plot_1.dat" using 1:2:(1.0) smooth sbezier with lines lc rgb "#ef7e18" lt 1 lw 2

set ylabel "T"
set output f2
plot "Plot_2.dat" using 1:2:(1.0) smooth sbezier with lines lc rgb "#9ac224" lt 2 lw 2

set ylabel "R"
set output f3
plot "Plot_3.dat" using 1:2:(1.0) smooth sbezier with lines lc rgb "#49ade0" lt 3 lw 2
#print "1"
