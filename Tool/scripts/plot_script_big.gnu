# Graphs 945 by 348
set xtics 10
set grid
unset key
set terminal png nocrop enhanced size 945,348
f1="../graphs/".filename."_Plot_1_b.png"
f2="../graphs/".filename."_Plot_2_b.png"
f3="../graphs/".filename."_Plot_3_b.png"

set xlabel "Time in X units"
set ylabel "Skin conductance in y units"
set title "Time vs Skin condition"
set output f1
plot "Plot_1.dat" using 1:2:(1.0) smooth sbezier with lines lc rgb "#ef7e18" lt 1 lw 2

set ylabel "Temperature in y units"
set title "Time vs Temperature"
set output f2
plot "Plot_2.dat" using 1:2:(1.0) smooth sbezier with lines lc rgb "#9ac224" lt 2 lw 2

set ylabel "Abdominal respiration in y units"
set title "Time vs Abdominal Respiration"
set output f3
plot "Plot_3.dat" using 1:2:(1.0) smooth sbezier with lines lc rgb "#49ade0" lt 3 lw 2
#print "1"
